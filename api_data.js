define({ "api": [
  {
    "type": "post",
    "url": "/auth/activation-code",
    "title": "Send Activation Code",
    "group": "Auth",
    "name": "Activation_Code",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>phoneNumber</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>phoneNumber</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "http://balad.show/api/v1/auth/activation-code"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/token",
    "title": "Get Token",
    "group": "Auth",
    "name": "Get_Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "emailOrPhoneNumber",
            "description": "<p>email or phone number.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT-Token.</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "http://balad.show/api/v1/auth/token"
      }
    ]
  },
  {
    "type": "put",
    "url": "/auth/refresh-token",
    "title": "Refresh Token",
    "group": "Auth",
    "name": "Refresh_Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>token.</p>"
          }
        ]
      }
    },
    "description": "<p>generate a new token from expired one.</p>",
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "http://balad.show/api/v1/auth/refresh-token"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/signup",
    "title": "SignUp",
    "group": "Auth",
    "name": "SignUp",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>PhoneNumber.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>PhoneNumber.</p>"
          }
        ]
      }
    },
    "description": "<p>verification code will be send after signup</p>",
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "http://balad.show/api/v1/auth/signup"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/verify-phone",
    "title": "Verify PhoneNumber",
    "group": "Auth",
    "name": "Verify_Phone_Number",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phoneNumber",
            "description": "<p>phoneNumber.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT_Token.</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "http://balad.show/api/v1/auth/verify-phone"
      }
    ]
  },
  {
    "type": "post",
    "url": "/auth/is-unique",
    "title": "check uniqueness of a field",
    "group": "Auth",
    "name": "check_uniqueness",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "field",
            "description": "<p>field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "value",
            "description": "<p>value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "unique",
            "description": "<p>is unique</p>"
          }
        ]
      }
    },
    "version": "1.0.0",
    "filename": "app/controllers/AuthController.ts",
    "groupTitle": "Auth",
    "sampleRequest": [
      {
        "url": "http://balad.show/api/v1/auth/is-unique"
      }
    ]
  },
  {
    "type": "get",
    "url": "/taxon/:id",
    "title": "Get Taxon",
    "group": "Taxonomy",
    "name": "Get_Taxon_by_id",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>Slug</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "position",
            "description": "<p>Position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "level",
            "description": "<p>Level</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "color",
            "description": "<p>Color</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lineage",
            "description": "<p>Lineage</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "parentId",
            "description": "<p>Parent Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "keywords",
            "description": "<p>Keywords</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "createdAt",
            "description": "<p>Creation Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updatedAt",
            "description": "<p>Update Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://balad.show/api/v1/taxon/2"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/TaxonController.ts",
    "groupTitle": "Taxonomy"
  },
  {
    "type": "get",
    "url": "/taxon",
    "title": "Get Taxons",
    "group": "Taxonomy",
    "name": "Get_Taxons",
    "description": "<p>This is the Description.</p> <p>Acceptable query string parameters: <code>parentId</code>, <code>level</code>, <code>levels</code></p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "taxons",
            "description": "<p>List of Taxons</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxons.id",
            "description": "<p>Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxons.slug",
            "description": "<p>Slug</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxons.position",
            "description": "<p>Position</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxons.level",
            "description": "<p>Level</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxons.color",
            "description": "<p>Color</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxons.title",
            "description": "<p>Title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "taxons.lineage",
            "description": "<p>Lineage</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "taxons.parentId",
            "description": "<p>Parent Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "taxons.keywords",
            "description": "<p>Keywords</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "taxons.createdAt",
            "description": "<p>Creation Date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "taxons.updatedAt",
            "description": "<p>Update Date</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://balad.show/api/v1/taxon?levels=1,2"
      }
    ],
    "version": "1.0.0",
    "filename": "app/controllers/TaxonController.ts",
    "groupTitle": "Taxonomy"
  }
] });
